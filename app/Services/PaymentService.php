<?php

namespace App\Services;


class PaymentService
{
    /**
     * @var \GuzzleHttp\Client
     */
    protected $client;

    /**
     * PaymentService constructor.
     */
    public function __construct()
    {
        $this->client = new \GuzzleHttp\Client();
    }

    /**
     * @param array $params
     * @return array
     */
    public function execute(array $params) : array
    {        
        $response = $this->client->post( env('URL_PAYMENT_SERVICE'), [
            'json' => $params
        ]);

        return json_decode( $response->getBody()->getContents(), true );
    }
}