<?php

namespace App\Repositories;


use App\Interfaces\Repositories\AddressInterface;
use App\Models\Address;
use App\Models\Customer;

class AddressRepository implements AddressInterface
{
    /**
     * @param array $params
     * @param Customer $customer
     * @return Address
     */
    public function store(Customer $customer, array $params): Address
    {
        $address = new Address($params);
        $address->customer()->associate($customer);
        $address->save();
        return $address;
    }
}