<?php

namespace App\Repositories;


use App\Interfaces\Repositories\CustomerInterface;
use App\Models\Customer;

class CustomerRepository implements CustomerInterface
{
    /**
     * @param array $params
     * @return Customer
     */
    public function store(array $params): Customer
    {
        return Customer::create($params);
    }
}