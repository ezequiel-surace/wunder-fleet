<?php

namespace App\Repositories;

use App\Interfaces\Repositories\PaymentInterface;
use App\Models\Customer;
use App\Models\Payment;
use App\Services\PaymentService;

class PaymentRepository implements PaymentInterface
{
    /**
     * @var PaymentService
     */
    protected $service;

    /**
     * PaymentRepository constructor.
     * @param PaymentService $paymentService
     */
    public function __construct(PaymentService $paymentService)
    {
        $this->service = $paymentService;
    }

    /**
     * @param array $params
     * @param Customer $customer
     * @return Payment
     */
    public function store(Customer $customer, array $params): Payment
    {
        $paramsService['customerId'] = $customer->id;
        $paramsService['iban'] = $params['iban'];
        $paramsService['owner'] = $params['account_owner'];

        $response = $this->service->execute($paramsService);


        $params['payment_data_id'] = $response['paymentDataId'];

        $payment = new Payment ($params);
        $payment->customer()->associate($customer);
        $payment->save();

        return $payment;
    }
}