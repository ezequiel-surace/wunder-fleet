<?php

namespace App\Http\Middleware;

use Closure;

class RedirectToStepMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        switch ($request->session()->get('step')) {
            case '2':
                return redirect('/addresses');
                break;
            case '3':
                return redirect('/payments');
                break;
        }

        return $next($request);
    }
}
