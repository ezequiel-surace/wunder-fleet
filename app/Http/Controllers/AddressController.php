<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddressRequest;
use App\Models\Address;
use App\Models\City;
use Illuminate\Http\Request;

class AddressController extends Controller
{
        /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->session()->put('step', 2);
        $cities = City::orderBy('name')->get();
        $address = $request->session()->get('address', []);

        return view('address.index', compact( 'cities', 'address' ) );
    }

    /**
     * @param AddressRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(AddressRequest $request)
    {
        $request->session()->put('address', $request->validated());
        return redirect('/payments');
    }

}
