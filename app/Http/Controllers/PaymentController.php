<?php

namespace App\Http\Controllers;

use App\Http\Requests\PaymentRequest;
use App\Interfaces\Repositories\AddressInterface;
use App\Interfaces\Repositories\CustomerInterface;
use App\Interfaces\Repositories\PaymentInterface;
use App\Models\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PaymentController extends Controller
{

    /**
     * @var CustomerInterface
     */
    protected $customers;

    /**
     * @var AddressInterface
     */
    protected $addresses;

    /**
     * @var PaymentInterface
     */
    protected $payments;

    /**
     * PaymentController constructor.
     * @param CustomerInterface $customerRepository
     * @param AddressInterface $addressRepository
     * @param PaymentInterface $paymentRepository
     */
    public function __construct(
        CustomerInterface $customerRepository,
        AddressInterface $addressRepository,
        PaymentInterface $paymentRepository
    )
    {
        $this->customers = $customerRepository;
        $this->addresses = $addressRepository;
        $this->payments = $paymentRepository;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $request->session()->put('step', 3);
        $payment = $request->session()->get('payment', []);
        return view('payment.index', compact('payment'));
    }

    /**
     * @param PaymentRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(PaymentRequest $request)
    {
        DB::beginTransaction();
        $customer = $this->customers->store( $request->session()->get('customer') );
        $this->addresses->store($customer, $request->session()->get('address'));
        $payment = $this->payments->store($customer, $request->validated());
        DB::commit();
        $request->session()->forget(['customer', 'address','step']);

        return redirect()->route('payments.show', $payment);
    }


    /**
     * @param Payment $payment
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Payment $payment){
        return view('payment.show', compact('payment'));
    }

}
