<?php

namespace App\Providers;

use App\Interfaces\Repositories\AddressInterface;
use App\Interfaces\Repositories\CustomerInterface;
use App\Interfaces\Repositories\PaymentInterface;
use App\Repositories\AddressRepository;
use App\Repositories\CustomerRepository;
use App\Repositories\PaymentRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(CustomerInterface::class, CustomerRepository::class);
        $this->app->bind(AddressInterface::class, AddressRepository::class);
        $this->app->bind(PaymentInterface::class, PaymentRepository::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
