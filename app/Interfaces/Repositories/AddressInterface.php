<?php

namespace App\Interfaces\Repositories;

use App\Models\Address;
use App\Models\Customer;

interface AddressInterface
{
    /**
     * @param array $params
     * @param Customer $customer
     *
     * @return Address
     */
    public function store(Customer $customer ,array $params) : Address;
}