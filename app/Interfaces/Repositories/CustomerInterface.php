<?php

namespace App\Interfaces\Repositories;

use App\Models\Customer;

interface CustomerInterface
{
    /**
     * @param array $params
     * @return Customer
     */
    public function store(array $params) : Customer;
}