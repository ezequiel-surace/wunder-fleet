<?php

namespace App\Interfaces\Repositories;

use App\Models\Customer;
use App\Models\Payment;

interface PaymentInterface
{
    /**
     * @param array $params
     * @param Customer $customer
     *
     * @return Payment
     */
    public function store(Customer $customer, array $params) : Payment;
}