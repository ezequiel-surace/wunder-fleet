1. Describe possible performance optimizations for your Code. 

a. Add a queue handler for the external API.

b. Use Reddis for the sessions, because is faster.

2. Which things could be done better, than you’ve done it? 

When I use the Payment Service I didn't do a "try and catch" to catch a possible 4xx error and return the correct view related with the error. I didn't have enough time because I am in Australia and I had to move two times.

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

I used Repository Pattern because :

1. It centralizes data logic or business logic and service logic.
2. Provides a flexible architecture.
3. If you want to modify the data access logic or business access logic, you don’t need to change the repository logic.
4. Easier to add and test new steps in the future.
