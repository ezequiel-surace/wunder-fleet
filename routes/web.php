<?php

use App\Http\Middleware\RedirectToStepMiddleware;

Route::get('/','CustomerController@index')->middleware(RedirectToStepMiddleware::class);


Route::resource('customers', 'CustomerController', [
    'only' => [ 'index', 'store' ]
]);

Route::resource('addresses', 'AddressController', [
    'only' => [ 'index', 'store' ]
]);

Route::resource('payments', 'PaymentController', [
    'only' => [ 'index', 'store', 'show' ]
]);
