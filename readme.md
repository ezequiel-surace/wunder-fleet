# Getting started

## Installation

Please check the official laravel installation guide for server requirements before you start. [Official Documentation](https://laravel.com/docs/5.8/installation#installation)


Clone the repository

    git clone https://ezequiel-surace@bitbucket.org/ezequiel-surace/wunder-fleet.git

Switch to the repo folder

    cd wunderfleet

Install all the dependencies using composer

    composer install

Copy the example env file and make the required configuration changes in the .env file

    cp .env.example .env

Generate a new application key

    php artisan key:generate

Run the database migrations (**Set the database connection in .env before migrating**)

    php artisan migrate

Start the local development server

    php artisan serve

You can now access the server at http://localhost:8000
    
**Make sure you set the correct database connection information before running the migrations** [Environment variables](#environment-variables)

    php artisan migrate
    php artisan serve

## Database seeding

Run the database seeder

    php artisan db:seed

***Note*** : It's recommended to have a clean database before seeding. You can refresh your migrations at any point to clean the database by running the following command

    php artisan migrate:refresh

# Code overview

## Folders

- `app/Models` - Contains all the Eloquent models
- `app/Http/Controllers/` - Contains all the controllers
- `app/Http/Middleware` - Contains the middleware to redirect to the correct step
- `app/Http/Requests` - Contains all the form requests
- `app/Http/Interfaces` - Contains all the interfaces
- `app/Http/Repositories` - Contains all the repositories
- `app/Http/Services` - Contains the PaymentService.php
- `database/migrations` - Contains all the database migrations
- `database/seeds` - Contains the database seeder
- `routes` - Contains all the routes defined in web.php file

## Environment variables

- `.env` - Environment variables can be set in this file

***Note*** : You can quickly set the database information and other variables in this file and have the application fully working.

----------

# Testing Wizard

Run the laravel development server

    php artisan serve

Put the following URL in the browser and test the wizard

    http://localhost:8000/


----------

