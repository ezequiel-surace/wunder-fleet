@extends('layouts.master')

@section('content')

<form action="/customers" method="POST" >
@csrf
    <div class="row">
        <div class="col-sm-6">       
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="material-icons">face</i>
                </span>
                <div class="form-group label-floating">
                    <label class="control-label">First Name <small>(required)</small></label>
                    <input name="first_name" type="text" class="form-control" value="{{ isset($customer['first_name']) ? $customer['first_name'] : '' }}">
                </div>
            </div>

            <div class="input-group">
                <span class="input-group-addon">
                    <i class="material-icons">record_voice_over</i>
                </span>
                <div class="form-group label-floating">
                    <label class="control-label">Last Name <small>(required)</small></label>
                    <input name="last_name" type="text" class="form-control" value="{{ isset($customer['last_name']) ? $customer['last_name'] : '' }}">
                </div>
            </div>
            
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="material-icons">phone</i>
                </span>
                <div class="form-group label-floating">
                    <label class="control-label">Phone <small>(required)</small></label>
                    <input name="phone" type="text" class="form-control" value="{{ isset($customer['phone']) ? $customer['phone'] : '' }}">
                </div>
            </div>
        </div>
    </div>

    <div class="wizard-footer">
            <div class="pull-right">
                <input type='submit' class='btn  btn-fill btn-success' name='next' value='Next' />
            </div>
            <div class="clearfix"></div>
    </div>
</form>

@endsection