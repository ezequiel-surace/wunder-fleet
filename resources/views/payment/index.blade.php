@extends('layouts.master')

@section('content') 

<form action="/payments" method="POST" >
@csrf

    <div class="tab-pane" id="payment">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-1">
                <div class="form-group label-floating">
                    <label class="control-label">Account owner <small>(required)</small></label>
                    <input name="account_owner" type="text" class="form-control">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-sm-offset-1">
                <div class="form-group label-floating">
                    <label class="control-label">IBAN <small>(required)</small></label>
                    <input name="iban" type="text" class="form-control">
                </div>
            </div> 
        </div>
    </div>

    <div class="wizard-footer">
        <div class="pull-right">
            <input type='submit' class='btn btn-fill btn-success' name='finish' value='Finish' />
        </div>
  
    <div class="pull-left">
        <a href="/addresses"><button type="button"  class="btn btn-fill" >Previous</button></a> 
    </div>

    </div>

</form>

@endsection