@extends('layouts.master')

@section('content')

    <h1 style="text-align: center;">Succesful!</h1> 
    <h3  style="text-align: left; margin-left: 10px;">Your Payment ID is :</h3>
    <p  style="text-align: center;">{{ $payment->payment_data_id }}</p>

    <div class="wizard-footer">
        <div class="pull-right">
            <a href="/"><button type="button"  class="btn btn-fill btn-success" >Add other customer</button></a>             
        </div>
        <div class="clearfix"></div>
    </div>
         
@endsection