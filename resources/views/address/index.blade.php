@extends('layouts.master')

@section('content') 

<form action="/addresses" method="POST" >
@csrf
    <div class="row">
        <div class="col-sm-7 col-sm-offset-1">
            <div class="form-group label-floating">
                <label class="control-label">Street Name <small>(required)</small></label>
                <input name="street_name" type="text" class="form-control" value="{{ isset($address['street_name']) ? $address['street_name'] : '' }}">
            </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group label-floating">
                <label class="control-label">Street Number <small>(required)</small></label>
                <input name="street_number" type="text" class="form-control" value="{{ isset($address['street_number']) ? $address['street_number'] : '' }}">
            </div>
        </div>
        <div class="col-sm-5 col-sm-offset-1">
            <div class="form-group label-floating">
                <label class="control-label">Zip Code <small>(required)</small></label>
                <input name="zip_code" type="text" class="form-control" value="{{ isset($address['zip_code']) ? $address['zip_code'] : '' }}">
            </div>
        </div>
        <div class="col-sm-5">
            <div class="form-group label-floating">
                <label class="control-label">City <small>(required)</small></label>
                <select name="city_id" class="form-control">
                    @if (isset($address['city_id']))
                    @foreach($cities as $key => $city)
                    @if ($city['id'] == $address['city_id'])
                    <option selected value="{{ $address['city_id'] }}"> {{ $city['name'] }} </option>
                    @php
                    unset($cities[$key])
                    @endphp
                    @endif
                    @endforeach
                    @else
                    <option selected disabled hidden></option>
                    @endif
                @foreach($cities as $city)

                    <option value="{{ $city['id'] }}"> {{ $city['name'] }} </option>

                @endforeach
                </select>
            </div>
        </div>
    </div>

    <div class="wizard-footer">
            <div class="pull-right">
                <input type='submit' class='btn btn-fill btn-success' name='next' value='Next' />
            </div>

            <div class="pull-left">
                <a href="/customers"><button type="button"  class="btn btn-fill" >Previous</button></a> 
            </div>
     
                   
    </div>
</form>

@endsection