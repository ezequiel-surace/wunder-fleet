<div class="footer">
    <div class="container text-center">
            Made with <i class="fa fa-heart heart"></i> by <a href="https://www.linkedin.com/in/ezequiel-surace-2b344652/">Ezequiel Surace</a>.
    </div>
</div>