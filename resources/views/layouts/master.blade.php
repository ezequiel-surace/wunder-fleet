<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<link rel="icon" type="image/jpg" href="img/favicon.png">

	<title>Profile</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


	<!--     Fonts and icons     -->
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

    <!-- CSS Files -->
    <link rel="stylesheet" type="text/css" href="{{ url('/css/bootstrap.min.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ url('/css/material-bootstrap-wizard.css') }}" />
    
</head>

<body>
<div class="image-container set-full-height" style="background-image: url('/img/background.jpg')">

	<!--   Big container   -->
<div class="container">
	<div class="row">
		<div class="col-sm-8 col-sm-offset-2">
			<!--      Wizard container        -->
			<div class="wizard-container">
				<div class="card wizard-card" data-color="green" id="wizardProfile">
						<div class="wizard-header">
							<h3 class="wizard-title">
								Build Your Profile
							</h3>
							<h5>This information will let us know more about you.</h5>
						</div>
						@yield('content')
				</div>
			</div> <!-- wizard container -->
		</div>
	</div><!-- end row -->
</div> <!--  big container -->
	    
@include('layouts.partials.footer')
		
</div>

</body>

<script src="js/jquery-2.2.4.min.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/jquery.bootstrap.js" type="text/javascript"></script>

<!--  Plugin for the Wizard -->
<script src="js/material-bootstrap-wizard.js"></script>

<!--  More information about jquery.validate here: http://jqueryvalidation.org/	 -->
<script src="js/jquery.validate.min.js"></script>


</html>
